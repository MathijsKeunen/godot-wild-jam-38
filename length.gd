# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends HBoxContainer

signal length_changed(new_length)

onready var label: Label = $number
onready var lower_button: TextureButton = $lower


func _ready() -> void:
	emit_signal("length_changed", get_length())


func _set_length(length: int) -> void:
	label.text = str(length)
	emit_signal("length_changed", length)
	lower_button.disabled = (length == 1)


func get_length() -> int:
	return int(label.text)


func _on_lower_pressed():
	_set_length(get_length() - 1)


func _on_higher_pressed():
	_set_length(get_length() + 1)

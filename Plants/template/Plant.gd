extends Machine
class_name Plant

signal points_received(points)

export var points_per_tick := 5

export var dehydrade_timeout := 7.0
export var die_timeout := 7.0

onready var plant_detection_area: Area2D = $rotation_node/Area2D
onready var timer: Timer = $Timer

var hydraded := false setget set_hydraded
var overlaps_green_area: bool setget , get_overlaps_green_area

export (AudioStreamSample) var produce_sound := preload("res://assets/sound effects/point received.wav")

func get_overlaps_green_area() -> bool:
	return len(plant_detection_area.get_overlapping_areas()) > 0

func set_hydraded(h: bool) -> void:
	hydraded = h
	if h:
		rotation_node.modulate = Color.white
	else:
		rotation_node.modulate = Color.crimson


func tick() -> void:
	if get_overlaps_green_area():
		timer.stop()
		_produce()
		set_hydraded(true)
	elif timer.time_left == 0:
		timer.start(dehydrade_timeout)


func _produce() -> void:
	emit_signal("points_received", points_per_tick)
	audio_player.stream = produce_sound
	audio_player.play()


func _on_Timer_timeout():
	if hydraded:
		set_hydraded(false)
		timer.start(die_timeout)
	else:
		queue_free()

# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Node2D
class_name Machine

signal activated
signal clicked(machine)

export (Dictionary) var production := {"water": 1.0}
export (Dictionary) var consumption := {"power": 1.0}
export (Dictionary) var storage := {"water": 30.0, "power": 60.0}

export (String) var machine_name: String
export (String) var info_string: String

onready var info_box = $info_box
var info_box_visible := false setget set_info_box_visible, get_info_box_visible

onready var rotation_node := $rotation_node

export (NodePath) var valid_detection_area_path: NodePath = "rotation_node/detection_area"
onready var valid_detection_area = get_node(valid_detection_area_path)

var contents := {}

var inputs: Dictionary
var outputs: Dictionary

var active := false setget set_active

onready var audio_player: AudioStreamPlayer2D = $AudioStreamPlayer2D
export (AudioStreamSample) var succes_sound: = preload("res://assets/sound effects/place.wav")
export (AudioStreamSample) var fail_sound := preload("res://assets/sound effects/place failed.wav")


func set_active(a: bool) -> void:
	active = a
	if a:
		audio_player.stream = succes_sound
		audio_player.play()
		emit_signal("activated")


func play_failed_sound() -> void:
	audio_player.stream = fail_sound
	audio_player.play()


func set_info_box_visible(v: bool) -> void:
	info_box.visible = v


func get_info_box_visible() -> bool:
	return info_box.visible


func set_rotation(r: float) -> void:
	rotation_node.set_rotation(r)


func get_rotation() -> float:
	return rotation_node.get_rotation()


func _ready() -> void:
	for k in storage.keys():
		contents[k] = 0.0
	info_box.target_node = self


func tick() -> void:
	if not active:
		return
	var can_produce = true
	for type in consumption.keys():
		if contents[type] >= consumption[type]:
			contents[type] -= consumption[type]
		else:
			contents[type] = 0.0
			can_produce = false
	if can_produce:
		_produce()


func _produce() -> void:
	for type in contents.keys():
		var amount: float = production.get(type, 0.0) + contents[type]
		if amount == 0:
			return
		var output_dict := {}
		var total_capacity := 0.0
		for output in outputs.get(type, []):
			var connection: IOPoint = output.connection
			if connection:
				var capacity = connection.get_capacity()
				output_dict[connection] = capacity
				total_capacity += capacity
		if total_capacity < amount:
			for connection in output_dict.keys():
				connection.push(output_dict[connection])
				contents[type] = min(amount - total_capacity, storage[type])
		else:
			var correction = amount / total_capacity
			for connection in output_dict.keys():
				connection.push(correction * output_dict[connection])


func register_IOPoint(point: IOPoint) -> void:
	if point.io == point.IO.input:
		inputs[point.type] = inputs.get(point.type, []) + [point]
	elif point.io == point.IO.output:
		outputs[point.type] = outputs.get(point.type, []) + [point]


func has_valid_location() -> bool:
	return len(valid_detection_area.get_overlapping_areas()) == 0


func _on_StaticBody2D_input_event(_viewport, event, _shape_idx):
	if active and event is InputEventMouseButton and event.pressed:
		emit_signal("clicked", self)


func _on_delete_button_pressed():
	audio_player.stream = succes_sound
	audio_player.play()
	for point_array in inputs.values():
		_remove_connections(point_array)
	for point_array in outputs.values():
		_remove_connections(point_array)
	visible = false
	yield(audio_player, "finished")
	queue_free()


func _remove_connections(points: Array) -> void:
	for p in points:
		var point := p as IOPoint
		if point and point.connection:
			point.connection.connection = null

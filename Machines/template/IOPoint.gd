# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Position2D
class_name IOPoint


enum IO {input, output}
export (IO) var io := IO.input

export (float) var rate := 1.0
export (String) var type := "water"

var connection: IOPoint

enum HIGHLIGH_MODE {off, discover, hover}

onready var dot := $AnimatedSprite

var capacity: float setget , get_capacity


func _ready() -> void:
	if owner:
		owner.register_IOPoint(self)
	
	if io == IO.input:
		add_to_group("input")
	elif io == IO.output:
		add_to_group("output")


func highlight(mode: int) -> void:
	if mode == HIGHLIGH_MODE.off:
		dot.play("off")
	elif mode == HIGHLIGH_MODE.discover:
		dot.play("discover")
	elif mode == HIGHLIGH_MODE.hover:
		dot.play("hover")


func get_capacity() -> float:
	capacity = min(rate, owner.storage[type] - owner.contents[type])
	return capacity


func push(amount: float) -> void:
	if amount > capacity:
		print("IOPoint: pushed amount is larger than capacity!")
	owner.contents[type] += min(amount, capacity)

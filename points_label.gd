extends Label

signal points_changed(new_points)

var points: int setget set_points, get_points


func set_points(p: int) -> void:
	if p < 0:
		print("points_label: tried to set negativa amount of points!")
	else:
		text = str(p)
		emit_signal("points_changed", p)


func get_points() -> int:
	return int(text)


func _ready() -> void:
	set_points(get_points())
	for b in get_tree().get_nodes_in_group("machine_button"):
		var button := b as MachineButton
		if button:
# warning-ignore:return_value_discarded
			connect("points_changed", button, "_on_points_label_points_changed")
	emit_signal("points_changed", get_points())


func _on_placed_elements_machine_added(machine: Plant):
	if machine:
# warning-ignore:return_value_discarded
		machine.connect("points_received", self, "_update_counter")


func _update_counter(amount: int) -> void:
	set_points(get_points() + amount)

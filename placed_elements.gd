# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Node2D

signal machine_added(machine)

var elapsed_time: float

export var tick_time := 2.0


func _process(delta: float) -> void:
	elapsed_time += delta
	if elapsed_time > tick_time:
		elapsed_time -= tick_time
		for c in get_children():
			var child = c as Machine
			if child:
				child.tick()


func add_child(c: Node, l := false) -> void:
	var child := c as Machine
	if child:
		.add_child(child, l)
		yield(child, "activated")
		emit_signal("machine_added", child)

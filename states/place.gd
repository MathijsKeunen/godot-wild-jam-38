# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends MainState
class_name PlaceState

signal points_spent(points)


onready var destination := $"../../placed_elements"
export (NodePath) var machine_buttons_path: NodePath
onready var machine_buttons = get_node(machine_buttons_path)
export (NodePath) var container_path: NodePath
onready var container := get_node(container_path)
onready var machine_buttons_group: ButtonGroup = machine_buttons.get_child(0).group
export (NodePath) var info_box_path: NodePath
onready var info_box: InfoBox = get_node(info_box_path)

var child setget set_child

var cost: int


func set_child(c) -> void:
	child = c
	if info_box:
		info_box.target_node = c


func _ready() -> void:
	for b in machine_buttons.get_children():
		var button = b as MachineButton
		if button:
			button.connect("pressed", self, "_on_MachineButton_pressed")


func _on_MachineButton_pressed():
	if child:
		remove_child(child)
		child.queue_free()
	set_child(_get_new_child())
	if child:
		add_child(child)


func enter() -> void:
	container.set_visible(true)
	set_child(_get_new_child())
	if child:
		add_child(child)
	.enter()


func _get_new_child() -> Machine:
	var button: MachineButton = machine_buttons_group.get_pressed_button()
	if button.disabled:
		return null
	cost = button.cost
	var child_scene = button.machine_scene
	return child_scene.instance()


func handle_input(e: InputEvent) -> void:
	if child:
		var button_event := e as InputEventMouseButton
		if button_event and button_event.pressed and\
		button_event.button_index == BUTTON_LEFT:
			if child.has_valid_location():
				remove_child(child)
				destination.add_child(child)
				child.active = true
				set_child(null)
				emit_signal("points_spent", -cost)
				emit_signal("finished")
			else:
				child.play_failed_sound()
		
		
		var motion_event := e as InputEventMouseMotion
		if motion_event:
			child.position = _get_local_position(motion_event.global_position)
			if child.has_valid_location():
				child.modulate = Color.white
			else:
				child.modulate = Color.crimson
	
	.handle_input(e)



func exit() -> void:
	container.set_visible(false)
	if child:
		remove_child(child)
		child.queue_free()
		set_child(null)

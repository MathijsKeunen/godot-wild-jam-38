# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends State
class_name MainState

onready var camera: Camera2D = $"../../Camera2D"


func handle_input(e: InputEvent) -> void:
	var event := e as InputEventMouseMotion
	if event and event.button_mask == BUTTON_RIGHT:
		camera.position -= event.relative
	.handle_input(e)

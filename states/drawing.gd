# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends State

signal points_spent(points)

onready var parent: State = get_parent()
#var line_scene = preload("res://Lines/template/Line.tscn")

var line: Line

var end_points: Array
var end_position: Vector2

onready var destination = $"../../../placed_elements"


func enter() -> void:
	line = parent.line_scene.instance()
	line.position = parent.start_point.to_global(Vector2.ZERO)
	parent.start_point.connection = line.start_point
	line.start_point.connection = parent.start_point
	destination.add_child(line)
	line.construct(parent.length)
	
	end_points = parent.get_IOPoints("input", parent.type)
	for point in end_points:
		point.highlight(1)
	.enter()


func handle_input(e: InputEvent) -> void:
	var motion_event := e as InputEventMouseMotion
	if motion_event:
		end_position = line.move_end_to(_get_local_position(motion_event.global_position))
		parent.highlight_close_points(end_position, end_points)
		if line.has_valid_location():
			line.modulate = Color.white
		else:
			line.modulate = Color.crimson
	
	var button_event := e as InputEventMouseButton
	if button_event and button_event.pressed and\
	button_event.button_index == BUTTON_LEFT:
		if line.has_valid_location():
			var point: IOPoint = parent.get_close_point(end_position, end_points)
			if point:
				var end_point: IOPoint = line.end_point
				point.connection = end_point
				end_point.connection = point
			
			line.active = true
			line = null
			emit_signal("points_spent", -parent.length * parent.cost)
			emit_signal("finished")
		else:
			line.play_failed_sound()
	.handle_input(e)


func exit() -> void:
	for point in end_points:
		point.highlight(0)
	if line:
		destination.remove_child(line)
		line.queue_free()
		line = null
		parent.start_point.connection = null
	.exit()

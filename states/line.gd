# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends MainState


onready var machines_path := $"../../placed_elements"

const click_distance := pow(50.0, 2.0)

onready var states_map := {"disabled": $disabled, "initial": $initial, "drawing": $drawing}
onready var state: State

var start_point: IOPoint

onready var line_controls: Control = $"../../UILayer/VBoxContainer/VBoxContainer"
onready var line_buttons := $"../../UILayer/VBoxContainer/VBoxContainer/line_buttons"
onready var line_buttons_group: ButtonGroup = line_buttons.get_child(0).group
onready var points_label: Label = $"../../UILayer/MarginContainer/points_texture/points_label"

var line_scene: Resource = preload("res://Lines/template/Line.tscn")
var type := "water"
var length: int

var cost: int


func _ready() -> void:
	for c in line_buttons.get_children():
		var child := c as MachineButton
		if child:
# warning-ignore:return_value_discarded
			child.connect("pressed", self, "_on_Machine_button_pressed")
# warning-ignore:return_value_discarded
			child.connect("disabled_changed", self, "_on_button_disabled_changed")


func _on_Machine_button_pressed() -> void:
	var button: LineButton = line_buttons_group.get_pressed_button()
	if button.disabled:
		_switch_state("disabled")
	else:
		cost = button.cost
		line_scene = button.machine_scene
		type = button.type
		_switch_state("initial")


func enter() -> void:
	line_controls.set_visible(true)
	if line_buttons_group.get_pressed_button().disabled:
		state = states_map["disabled"]
	else:
		state = states_map["initial"]
	state.enter()
	.enter()


func handle_input(e: InputEvent) -> void:
	state.handle_input(e)
	.handle_input(e)


func highlight_close_points(pos: Vector2, points: Array) -> void:
	for p in points:
		var point = p as IOPoint
		if point:
			if point.global_position.distance_squared_to(pos) < click_distance:
				point.highlight(2)
			else:
				point.highlight(1)


func get_close_point(pos: Vector2, points: Array) -> IOPoint:
	for p in points:
		var point = p as IOPoint
		if point and point.global_position.distance_squared_to(pos) < click_distance:
			return point
	return null


# warning-ignore:shadowed_variable
func get_IOPoints(group := "output", type := "water") -> Array:
	var points := []
	for p in get_tree().get_nodes_in_group(group):
		var point := p as IOPoint
		if point and point.type == type and not point.connection:
			points.append(point)
	return points


func exit() -> void:
	state.exit()
	state = null
	line_controls.set_visible(false)
	
	.exit()


func _switch_state(new_state: String) -> void:
	if state:
		state.exit()
		state = states_map[new_state]
		state.enter()


func _on_initial_point_selected(point):
	start_point = point
	_switch_state("drawing")


func _on_drawing_finished():
	emit_signal("finished")


func _on_length_length_changed(new_length):
	length = new_length
	if state != states_map["disabled"]:
		_switch_state("initial")


func _on_button_disabled_changed(_d: bool) -> void:
	if line_buttons_group.get_pressed_button().disabled:
		_switch_state("disabled")

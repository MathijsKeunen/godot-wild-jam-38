# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends State

signal point_selected(point)

var start_points: Array

onready var parent: State = get_parent()


func enter() -> void:
	start_points = parent.get_IOPoints("output", parent.type)
	
	for point in start_points:
		point.highlight(1)
	.enter()


func handle_input(e: InputEvent) -> void:
	var motion_event = e as InputEventMouseMotion
	if motion_event:
		parent.highlight_close_points(_get_local_position(motion_event.position), start_points)
	
	var button_event = e as InputEventMouseButton
	if button_event and button_event.pressed and button_event.button_index == BUTTON_LEFT:
		var point: IOPoint = parent.get_close_point(_get_local_position(button_event.global_position), start_points)
		if point:
			emit_signal("point_selected", point)
	.handle_input(e)


func exit() -> void:
	for point in start_points:
		point.highlight(0)
	.exit()

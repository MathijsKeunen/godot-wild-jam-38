# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends PlaceState

var angle := 0
const angle_speed := 0.2


func handle_input(e: InputEvent) -> void:
	if child:
		var button_event := e as InputEventMouseButton
		if button_event and button_event.button_index == BUTTON_WHEEL_UP:
			child.set_rotation(child.get_rotation() - angle_speed)
		
		elif button_event and button_event.button_index == BUTTON_WHEEL_DOWN:
			child.set_rotation(child.get_rotation() + angle_speed)
	.handle_input(e)

# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends MainState

var selected_machine: Machine

var active := false


func enter() -> void:
	active = true
	.enter()


func _on_placed_elements_machine_added(machine: Machine) -> void:
# warning-ignore:return_value_discarded
	machine.connect("clicked", self, "_on_machine_clicked")


func _on_machine_clicked(machine: Machine) -> void:
	if active:
		if is_instance_valid(selected_machine):
			selected_machine.info_box_visible = false
		selected_machine = machine
		selected_machine.info_box_visible = true


func exit() -> void:
	if is_instance_valid(selected_machine):
		selected_machine.info_box_visible = false
		selected_machine = null
	active = false
	.exit()

# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Node2D


onready var states_map := {"normal": $normal, "machine": $machine, "line": $line, "plant": $plant}
enum ACTION_BUTTON {normal, machine, line, plant}

onready var current_state: State = states_map["normal"]

signal state_changed(new_state)


func _ready() -> void:
	for child in get_children():
		child.connect("finished", self, "_switch_state", ["normal"])


func _on_button_pressed(action: int) -> void:
	if action == ACTION_BUTTON.normal:
		_switch_state("normal")
	elif action == ACTION_BUTTON.machine:
		_switch_state("machine")
	elif action == ACTION_BUTTON.line:
		_switch_state("line")
	elif action == ACTION_BUTTON.plant:
		_switch_state("plant")


func _switch_state(new_state: String) -> void:
	if states_map[new_state] != current_state:
		current_state.exit()
		current_state = states_map[new_state]
		current_state.enter()
		emit_signal("state_changed", new_state)


func _unhandled_input(event: InputEvent) -> void:
	if current_state:
		current_state.handle_input(event)

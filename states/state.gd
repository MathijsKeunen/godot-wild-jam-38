# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Node2D
class_name State

# warning-ignore:unused_signal
signal finished


func enter() -> void:
	pass


func handle_input(_e: InputEvent) -> void:
	pass


func exit() -> void:
	pass


func _get_local_position(p: Vector2) -> Vector2:
	return p - get_viewport_transform().origin

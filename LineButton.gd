# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends MachineButton
class_name LineButton

signal disabled_changed(d)


export var type := "water"

onready var length_label := $"../../length"

var points: int
var length: int


func _ready() -> void:
# warning-ignore:return_value_discarded
	length_label.connect("length_changed", self, "_on_length_changed")


func _on_length_changed(l: int) -> void:
	update_disabled(-1, l)


func _on_points_label_points_changed(p: int) -> void:
	update_disabled(p)


func update_disabled(p := -1, l := -1):
	if p >= 0:
		points = p
	if l >= 0:
		length = l
	var new_disabled = points < cost * length
	if disabled != new_disabled:
		emit_signal("disabled_changed", new_disabled)
	disabled = new_disabled

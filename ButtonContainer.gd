# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends HBoxContainer


onready var button_map := {"normal": $normal, "machine": $machine, "line": $line, "plant": $plant}

#onready var group: ButtonGroup = get_child(0).group

func _on_states_state_changed(new_state: String):
	button_map[new_state].set_pressed(true)

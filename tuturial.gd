extends Control

signal scrolled
signal clicked
signal right_dragged

export var enabled := true


func start() -> void:
	set_process_input(false)
	if not enabled:
		return
	
	$construction.visible = true
	yield($"../VBoxContainer/state_buttons/machine", "pressed")
	$construction.visible = false
	
	$"wind turbine".visible = true
	yield($"../VBoxContainer/VBoxContainer2/machine_buttons/wind turbine", "pressed")
	$"wind turbine".visible = false
	
	$"click to construct".visible = true
	yield($"../../placed_elements", "machine_added")
	$"click to construct".visible = false
	
	$construction.visible = true
	yield($"../VBoxContainer/state_buttons/machine", "pressed")
	$construction.visible = false
	
	$"water condenser".visible = true
	yield($"../VBoxContainer/VBoxContainer2/machine_buttons/water condenser", "pressed")
	$"water condenser".visible = false
	
	$scroll.visible = true
	set_process_input(true)
	yield(self, "scrolled")
	set_process_input(false)
	$scroll.visible = false

	
	$"click to construct".visible = true
	yield($"../../placed_elements", "machine_added")
	$"click to construct".visible = false
	
	$line.visible = true
	yield($"../VBoxContainer/state_buttons/line", "pressed")
	$line.visible = false
	
	$power.visible = true
	yield($"../VBoxContainer/VBoxContainer/line_buttons/power", "pressed")
	$power.visible = false
	
	$connection1.visible = true
	yield($"../../states/line/initial", "point_selected")
	$connection1.visible = false
	
	$connection2.visible = true
	yield($"../../placed_elements", "machine_added")
	$connection2.visible = false
	
	$drag.visible = true
	set_process_input(true)
	yield(self, "right_dragged")
	set_process_input(true)
	$drag.visible = false
	
	$construction.visible = true
	yield($"../VBoxContainer/state_buttons/machine", "pressed")
	$construction.visible = false
	
	$sprinkler.visible = true
	yield($"../VBoxContainer/VBoxContainer2/machine_buttons/sprinkler", "pressed")
	$sprinkler.visible = false
	
	$"click to construct".visible = true
	yield($"../../placed_elements", "machine_added")
	$"click to construct".visible = false
	
	$line.visible = true
	yield($"../VBoxContainer/state_buttons/line", "pressed")
	$line.visible = false
	
	$water.visible = true
	yield($"../VBoxContainer/VBoxContainer/line_buttons/water", "pressed")
	$water.visible = false
	
	$connection3.visible = true
	yield($"../../states/line/initial", "point_selected")
	$connection3.visible = false
	
	$connection4.visible = true
	yield($"../../placed_elements", "machine_added")
	$connection4.visible = false
	
	$"wind turbine2".visible = true
	yield($"../../placed_elements", "machine_added")
	$"wind turbine2".visible = false
	
	$connection7.visible = true
	yield($"../../placed_elements", "machine_added")
	$connection7.visible = false
	
	$plant.visible = true
	yield($"../VBoxContainer/state_buttons/plant", "pressed")
	$plant.visible = false
	
	$connection5.visible = true
	yield($"../../placed_elements", "machine_added")
	$connection5.visible = false
	
	$connection6.visible = true
	set_process_input(true)
	yield(self, "clicked")
	yield(self, "clicked")
	set_process_input(false)
	$connection6.visible = false
	
	$connection8.visible = true
	set_process_input(true)
	yield(self, "clicked")
	yield(self, "clicked")
	set_process_input(false)
	$connection8.visible = false


func _input(e: InputEvent) -> void:
	var button_event := e as InputEventMouseButton
	if button_event:
		if button_event.button_index == BUTTON_WHEEL_UP or button_event.button_index == BUTTON_WHEEL_DOWN:
			emit_signal("scrolled")
		elif button_event.button_index == BUTTON_LEFT:
			emit_signal("clicked")
	
	var motion_event := e as InputEventMouseMotion
	if motion_event and motion_event.button_mask == BUTTON_RIGHT:
		emit_signal("right_dragged")


func _on_CheckButton_toggled(pressed: bool) -> void:
	enabled = pressed

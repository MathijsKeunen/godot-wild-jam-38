# Copyright 2021 Mathijs Keunen

# This file is part of Desert Shaper.

# Desert Shaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Desert Shaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Desert Shaper.  If not, see <https://www.gnu.org/licenses/>.
extends Machine
class_name Line


onready var end: PhysicsBody2D = $rotation_node/end

onready var last_joint: LineJoint = $rotation_node/DampedSpringJoint2D

onready var spring_length: float = last_joint.length

var length := 2

export var end_point_path: NodePath
var end_point: IOPoint setget , get_end_point

func get_end_point() -> IOPoint:
	return get_node(end_point_path) as IOPoint

export var start_point_path: NodePath
var start_point: IOPoint setget , get_start_point

func get_start_point() -> IOPoint:
	return get_node(start_point_path) as IOPoint

export var joint_path: Resource = preload("res://Lines/template/LineJoint.tscn")
export var segment_path: Resource = preload("res://Lines/template/LineSegment.tscn")

func set_active(a: bool) -> void:
	if a:
		end.set_collision_mask_bit(2, false)
	.set_active(a)


func construct(new_length := 2) -> void:
	for _i in range(new_length - 2):
		_add_segment()
	length = new_length


func _add_segment() -> void:
	var joint: LineJoint = joint_path.instance()
	var segment: LineSegment = segment_path.instance()
	$rotation_node.add_child(segment)
	$rotation_node.add_child(joint)
	joint.node_b = last_joint.node_b
	last_joint.node_b = segment.get_path()
	joint.node_a = segment.get_path()
	last_joint = joint
# warning-ignore:return_value_discarded
	segment.connect("input_event", self, "_on_StaticBody2D_input_event")


func move_end_to(pos: Vector2) -> Vector2:
	end.position = to_local(pos).clamped((length - 1) * (spring_length + 25))
	return to_global(end.position)

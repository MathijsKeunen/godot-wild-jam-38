extends PanelContainer
class_name InfoBox

export (bool) var show_production := true #setget set_show_production
export (bool) var show_consumption := true #setget set_show_consumption
export (bool) var show_storage := true #setget set_show_storage
export (bool) var show_top_buttons := false setget set_show_top_buttons

onready var name_label: Label = $VBoxContainer/name
onready var desc_label: Label = $VBoxContainer/description
onready var production_label: Label = $VBoxContainer/production_label
onready var production: ItemList = $VBoxContainer/production
onready var consumption_label: Label = $VBoxContainer/consumption_label
onready var consumption: ItemList = $VBoxContainer/consumption
onready var storage_label: Label = $VBoxContainer/storage_label
onready var storage: ItemList = $VBoxContainer/storage

var target_node: Machine setget set_target_node


func _ready():
	production.visible = show_production
	production_label.visible = show_production
	consumption_label.visible = show_consumption
	consumption.visible = show_consumption
	storage_label.visible = show_storage
	storage.visible = show_storage


func set_show_production(v: bool) -> void:
	if not production:
		return
	show_production = v
	$VBoxContainer/prodution_label.visible = v
	production.visible = v


func set_show_consumption(v: bool) -> void:
	if not consumption:
		return
	show_consumption = v
	$VBoxContainer/consumption_label.visible = v
	consumption.visible = v


func set_show_storage(v: bool) -> void:
	if not storage:
		return
	show_storage = v
	$VBoxContainer/storage_label.visible = v
	storage.visible = v


func set_show_top_buttons(v: bool) -> void:
	$VBoxContainer/ButtonContainer.visible = v


func set_target_node(m: Machine) -> void:
	if not m:
		visible = false
		return
	visible = true
	target_node = m
	name_label.text = m.machine_name
	desc_label.text = m.info_string
	if show_production:
		_fill_itemlist(production, m.production)
	if show_consumption:
		_fill_itemlist(consumption, m.consumption)
	if show_storage:
		_fill_itemlist(storage, m.storage)


func _fill_itemlist(list: ItemList, dict: Dictionary) -> void:
	list.clear()
	for key in dict.keys():
		var prop_string: String = str(dict[key]) + " units of " + key
		list.add_item(prop_string, null, false) 


func _on_close_button_pressed():
	visible = false
